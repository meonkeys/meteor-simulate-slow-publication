Meteor.publish('people', function() {
  Meteor._sleepForMs(2000);
  return People.find();
});
