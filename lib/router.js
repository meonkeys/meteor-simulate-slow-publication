Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

Router.map(function() {
  return this.route('home', {
    path: '/',
    waitOn: function() {
      return [Meteor.subscribe('people')];
    },
    data: function() {
      return {
        'people': People.find()
      };
    }
  });
});

Router.onBeforeAction('loading');
